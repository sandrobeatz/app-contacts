import React, {Component} from 'react';

class Avatar extends Component {
    render() {
        return(
            <div className="img-wrapper">
                <img src={this.props.image} alt={this.props.alt}/>
            </div>
        )
    }
}



export default Avatar