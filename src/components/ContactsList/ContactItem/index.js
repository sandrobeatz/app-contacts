import React, {Component} from 'react';
import Avatar from "./Avatar";

class ContactItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <li onClick={this.props.handleClick}>
                <Avatar
                    image={this.props.data.avatar}
                    alt={this.props.data.name}/>
                <p className="name">{this.props.data.name}</p>
                <p className="user-name"><span>@</span>{this.props.data.username}</p>
                <p className="sub-title">{this.props.data.address.state}</p>
            </li>
        )
    }
}



export default ContactItem