import React, {Component} from 'react';
import ContactItem from "./ContactItem"
const API = 'http://demo.sibers.com/users';

class ContactsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hits: []
        };

        this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {
        console.log(21);
    }

    componentWillMount() {
        $.ajax({
            url: API,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({hits: data});
                console.log(this.state);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
                console.log('fail');
            }.bind(this)
        });
    }

    render() {

        const { hits } = this.state;
        return (
            <ul className="list-contacts">
                {hits.map(hit =>
                    <ContactItem
                        key={hit.id}
                        data={hit}
                        handleClick={this.handleClick}/>
                )}

                <span id="loader-icon">load more...</span>
            </ul>
        )
    }
}

export default ContactsList