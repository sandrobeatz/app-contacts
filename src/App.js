import React from "react";
import Loader from "./components/Loader"
import ContactsList from "./components/ContactsList"

class App extends React.Component {

    // Constructor
    constructor() {
        super();

        this.state = {
            isLoading: true
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                isLoading: false
            })
        }, 1500)
    }

    render () {
        if(this.state.isLoading === true) {
            return (
                <Loader />
            )
        }
        return (
            <div className="contact-app">
                <header className="contact-app--header"></header>
                <div className="contact-app--body">
                    <div className="container">

                        <div className="row">
                            <div className="col-md-6">
                                <ContactsList/>
                            </div>
                            <div className="col-md-6"></div>
                        </div>

                    </div>
                </div>
                <footer className="contact-app--footer"></footer>
            </div>
        )


    }
}

export default App;