import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";
import './scss/app.scss';
import {
    Container,
    Icon,
    Link,
    List,
    ListItem,
    Offcanvas,
    OffcanvasContainer,
    Navbar,
    NavbarContainer,
    NavbarSticky,
    Section,
} from 'uikit-react';

ReactDOM.render(<App />, document.getElementById("root"));